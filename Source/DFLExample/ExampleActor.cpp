﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "ExampleActor.h"
#include "DebugFunctionLibrary/Public/DFLDebugLog.h"
#include "DFLExampleLog.h"
#include "Kismet/KismetMathLibrary.h"

AExampleActor::AExampleActor()
{
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(Root);

	TextRenderComponent = CreateDefaultSubobject<UTextRenderComponent>(TEXT("Text"));
	TextRenderComponent->SetupAttachment(Root);
}

void AExampleActor::BeginPlay()
{
	Super::BeginPlay();
}

void AExampleActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FVector Axis = UKismetMathLibrary::RotateAngleAxis(FVector::UpVector, 10.0f * GetWorld()->GetRealTimeSeconds(), FVector::RightVector);
	const FVector MovingEnd = FVector(10 * GetWorld()->GetRealTimeSeconds(), 0, 100);
	FRotator Rotation = FRotator(0, 0, 10 * GetWorld()->GetRealTimeSeconds());

	// TICK DRAW LINE
	// DFLTDRAW_LINE(DFLVZERO, MovingEnd, DRAW_PRIMARY_COLOUR); 
	// DFLSTDRAW_LINE(DFLVZERO, MovingEnd, DFLRED, 1);
	
	// TICK DRAW BOX
	DFLTDRAW_BOX(DFLVZERO, FVector(50), Rotation, DRAW_PRIMARY_COLOUR); 
	// DFLSTDRAW_BOX(DFLVZERO, FVector(50), Rotation, DFLRED, 1);
	
	// TICK DRAW CONE
	// DFLTDRAW_CONE(DFLVONE, DFLVUP, 45, 45, DRAW_PRIMARY_COLOUR); 
	// DFLSTDRAW_CONE(DFLVONE, DFLVUP, 50, 45, 45, 12, DFLRED, 1);
	
	// TICK DRAW ARROW
	// DFLTDRAW_ARROW(DFLVZERO, FVector(0, 0, 100), DRAW_PRIMARY_COLOUR); 
	// DFLSTDRAW_ARROW(DFLVZERO, FVector(0, 0, 100), 20, DFLRED, 1);
	
	// TICK DRAW PLANE
	// DFLTDRAW_PLANE(DFLVZERO, Axis, DRAW_PRIMARY_COLOUR); 
	// DFLSTDRAW_PLANE(DFLVZERO, Axis, 50, DFLRED);
	
	// TICK DRAW POINT
	// DFLTDRAW_POINT(FVector::ZeroVector, DRAW_PRIMARY_COLOUR); 
	// DFLSTDRAW_POINT(DFLVZERO, 20, DFLRED);
	
	// TICK DRAW CIRCLE
	// DFLTDRAW_CIRCLE(DFLVZERO, Axis, DFLVUP, DRAW_PRIMARY_COLOUR); 
	// DFLSTDRAW_CIRCLE(DFLVZERO, 20, 12, DFLRED, 1, Axis, DFLVUP);
	
	// TICK DRAW SPHERE
	// DFLTDRAW_SPHERE(FVector::ZeroVector, DRAW_PRIMARY_COLOUR); 
	// DFLSTDRAW_SPHERE(DFLVZERO, 20, 12, DFLRED, 1);
	
	// TICK DRAW CAPSULE
	// DFLTDRAW_CAPSULE(FVector::ZeroVector, Rotation, DRAW_PRIMARY_COLOUR); 
	// DFLSTDRAW_CAPSULE(DFLVZERO, 80, 20, Rotation, DFLRED, 1);
	
	// TICK DRAW CYLINDER
	// DFLTDRAW_CYLINDER(FVector::ZeroVector, FVector(0, 0, 100), DRAW_PRIMARY_COLOUR); 
	// DFLSTDRAW_CYLINDER(DFLVZERO, FVector(0, 0, 100), 20, 12, DFLRED, 1);
	
	// TICK DRAW FRUSTUM
	// DFLTDRAW_FRUSTUM(SomeTransform, DRAW_PRIMARY_COLOUR); 
	// DFLSTDRAW_FRUSTRUM(SomeTransform, DFLRED, 1);
	
	// TICK DRAW STRAIGHT LINE
	// DFLTDRAW_STRAIGHT_LINE(DFLVZERO, DRAW_PRIMARY_COLOUR); 
	// DFLSTDRAW_STRAIGHT_LINE(DFLVZERO, 50, DFLRED, 1);
	
	// TICK DRAW LINE DIRECTION
	// DFLTDRAW_LINE_DIRECTION(FVector::ZeroVector, Axis, DRAW_PRIMARY_COLOUR); 
	// DFLSTDRAW_LINE_DIRECTION(DFLVZERO, Axis, 50, DFLRED, 1);
}

void AExampleActor::TestPrint()
{
	// ======== PRINT ======== //

	// PRINT AND LOG
	// DFLPrint(DFLExampleLog, "Print and log");
	// DFLPrintW(DFLExampleLog, "Warning Print and log");
	DFLPrintE(DFLExampleLog, "Error Print and log");

	// PRINT AND LOG FORMATTED
	// DFLPrintF(DFLExampleLog, "Print and log format %f", SomeValue);
	// DFLPrintWF(DFLExampleLog, "Warning Print and log format %f", SomeValue);
	// DFLPrintEF(DFLExampleLog, "Error Print and log format %f", SomeValue);

	// STATIC PRINT AND LOG
	// DFLSPrint(DFLExampleLog, 2.0f, "Static Print and log");
	// DFLSPrintW(DFLExampleLog, 2.0f, "Static Warning Print and log");
	// DFLSPrintE(DFLExampleLog, 2.0f, "Static Error Print and log");

	// STATIC PRINT AND LOG FORMATTED
	// DFLSPrintF(DFLExampleLog, 2.0f, "Static Print and log format %f", SomeValue);
	// DFLSPrintWF(DFLExampleLog, 2.0f, "Static Warning Print and log format %f", SomeValue);
	// DFLSPrintEF(DFLExampleLog, 2.0f, "Static Error Print and log format %f", SomeValue);

	// QUICK PRINT AND LOG 
	// DFLQPrint("Quick Print and log");
	// DFLQPrintW("Warning Quick Print and log");
	// DFLQPrintE("Error Quick Print and log");

	// QUICK PRINT AND LOG FORMATTED
	// DFLQPrintF("Quick Print and log %f", SomeValue);
	// DFLQPrintWF("Warning Quick Print and log %f", SomeValue);
	// DFLQPrintEF("Error Quick Print and log %f", SomeValue);
}

void AExampleActor::TestLog()
{
	// ======== LOG ======== //
	
	// LOG 
	// DFLLog(DFLExampleLog, "Log");
	DFLLogW(DFLExampleLog, "Warning Log");
	// DFLLogE(DFLExampleLog, "Error Log");

	// LOG FORMATTED
	// DFLLogF(DFLExampleLog, "Log format %f", SomeValue);
	// DFLLogWF(DFLExampleLog, "Warning Log format %f", SomeValue);
	// DFLLogEF(DFLExampleLog, "Error Log format %f", SomeValue);

	// STATIC LOG
	// DFLSLog(DFLExampleLog, "Log format %f");
	// DFLSLogW(DFLExampleLog, "Warning Log format %f");
	// DFLSLogE(DFLExampleLog, "Error Log format %f");

	// STATIC LOG FORMATTED
	// DFLSLogF(DFLExampleLog, "Log format %f", SomeValue);
	// DFLSLogWF(DFLExampleLog, "Warning Log format %f", SomeValue);
	// DFLSLogEF(DFLExampleLog, "Error Log format %f", SomeValue);
}

void AExampleActor::TestDraw()
{
	// ======== DRAW DEBUG ======== //
	
	// DRAW ACTOR
	// DFLDRAW_ACTOR(SomeActor); 
	// DFLSDRAW_ACTOR(SomeActor, 50, 2, 1);
	
	// DRAW BOX
	// DFLDRAW_BOX(FVector::ZeroVector, FVector(50), FRotator::ZeroRotator, DRAW_PRIMARY_COLOUR); 
	// DFLSDRAW_BOX(FVector::ZeroVector, FVector(50), FRotator::ZeroRotator, FLinearColor::Red, 2, 1);
	
	// DRAW LINE
	DFLDRAW_LINE(FVector::ZeroVector, FVector(0, 0, 100), DRAW_PRIMARY_COLOUR); 
	// DFLSDRAW_LINE(FVector(150, 0, 0), FVector(150, 0, 100), FLinearColor::Red, 2, 1);
	
	// DRAW STRAIGHT LINE
	// DFLDRAW_STRAIGHT_LINE(FVector::ZeroVector, DRAW_SECONDARY_COLOUR); 
	// DFLSDRAW_STRAIGHT_LINE(FVector::ZeroVector, 50, FLinearColor::Red, 10, 1);
	
	// DRAW LINE DIRECTION
	// DFLDRAW_LINE_DIRECTION(FVector::ZeroVector, FVector::ForwardVector, DRAW_TERTIARY_COLOUR); 
	// DFLSDRAW_LINE_DIRECTION(FVector(0, 0, 0), FVector::ForwardVector, 50, FLinearColor::Red, 10, 1);
	
	// DRAW CIRCLE
	// DFLDRAW_CIRCLE(FVector::ZeroVector, FVector::RightVector, FVector::UpVector, DRAW_PRIMARY_COLOUR); 
	// DFLSDRAW_CIRCLE(FVector(100, 0, 0), 20, 12, FLinearColor::Red, 2, 1, FVector::RightVector, FVector::UpVector);
	
	// DRAW POINT
	// DFLDRAW_POINT(FVector::ZeroVector, DRAW_PRIMARY_COLOUR); 
	// DFLSDRAW_POINT(FVector::ZeroVector, 20, FLinearColor::Red, 2);
	
	// DRAW ARROW
	// DFLDRAW_ARROW(FVector::ZeroVector, FVector(0, 0, 100), DRAW_PRIMARY_COLOUR); 
	// DFLSDRAW_ARROW(FVector::ZeroVector, FVector(0, 0, 100), 20, FLinearColor::Red, 2, 1);
	
	// DRAW PLANE
	// DFLDRAW_PLANE(FVector::ZeroVector, FVector::UpVector, DRAW_SECONDARY_COLOUR); 
	// DFLSDRAW_PLANE(FVector::ZeroVector, FVector::UpVector, 50, FLinearColor::Red, 2);
	
	// DRAW SPHERE
	// DFLDRAW_SPHERE(FVector::ZeroVector, DRAW_PRIMARY_COLOUR); 
	// DFLSDRAW_SPHERE(FVector::ZeroVector, 20, 12, FLinearColor::Red, 2, 1);
	
	// DRAW CYLINDER
	// DFLDRAW_CYLINDER(FVector::ZeroVector, FVector(0, 0, 100), DRAW_PRIMARY_COLOUR); 
	// DFLSDRAW_CYLINDER(FVector::ZeroVector, FVector(0, 0, 100), 20, 12, FLinearColor::Red, 2, 1);
	
	// DRAW CONE
	// DFLDRAW_CONE(FVector::ZeroVector, FVector::UpVector, 45, 45, DRAW_PRIMARY_COLOUR); 
	// DFLSDRAW_CONE(FVector::ZeroVector, FVector::UpVector, 50, 45, 45, 12, FLinearColor::Red, 2, 1);
	
	// DRAW CAPSULE
	// DFLDRAW_CAPSULE(FVector::ZeroVector, FRotator::ZeroRotator, DRAW_TERTIARY_COLOUR);
	// DFLSDRAW_CAPSULE(FVector::ZeroVector, 80, 20, FRotator::ZeroRotator, FLinearColor::Green, 2, 1);
	
	// DRAW FRUSTUM
	// DFLDRAW_FRUSTUM(SomeTransform, DRAW_PRIMARY_COLOUR);
	// DFLSDRAW_FRUSTUM(SomeTransform, FLinearColor::Red, 2, 1);
	
	// DRAW CAMERA
	// DFLDRAW_CAMERA(Camera, DRAW_PRIMARY_COLOUR);
	// DFLSDRAW_CAMERA(Camera, 2, FLinearColor::Red, 2);
}
