﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "CoreMinimal.h"
#include "Components/TextRenderComponent.h"
#include "GameFramework/Actor.h"
#include "DebugFunctionLibrary/Public/DFLDebugTypes.h"
#include "ExampleActor.generated.h"

class UCameraComponent;

UCLASS()
class DFLEXAMPLE_API AExampleActor : public AActor
{
	GENERATED_BODY()

public:
	AExampleActor();

	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

public:
	UPROPERTY(BlueprintReadOnly, VisibleDefaultsOnly)
	USceneComponent* Root;
	
	UPROPERTY(BlueprintReadOnly, VisibleDefaultsOnly)
	UCameraComponent* Camera;

	UPROPERTY(BlueprintReadOnly, VisibleDefaultsOnly)
	UTextRenderComponent* TextRenderComponent;

	// Actor to draw debug
	UPROPERTY(EditAnywhere, Category = "Debug")
	AActor* SomeActor;

	// This is necessary to use the DFL draw debug macros. The debug properties for the macros running on this actor come from this struct.
	UPROPERTY(EditAnywhere, Category = "Debug")
	FDFLDebugProperties DebugProperties;
	
	UFUNCTION(BlueprintCallable)
	void TestPrint();

	UFUNCTION(BlueprintCallable)
	void TestLog();
	
	UFUNCTION(BlueprintCallable)
	void TestDraw();
	
private:
	float SomeValue = 2.0f;
	FTransform SomeTransform = FTransform(FRotator::ZeroRotator, FVector::ZeroVector, FVector(2));

};
