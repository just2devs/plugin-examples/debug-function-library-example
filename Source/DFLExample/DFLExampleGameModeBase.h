// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DFLExampleGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class DFLEXAMPLE_API ADFLExampleGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
